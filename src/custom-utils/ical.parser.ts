import { IEventTime } from "@/interfaces/ical.interface";

export const parseNextWord = (data: string, word: string) => {
  const index = data.indexOf(word);
  if (index === -1) {
    return null;
  }

  const endIndex = data.indexOf('\n', index);
  const response = data.slice(index + word.length, endIndex).trim();

  return response;
};

export const formatDateTime = (dateTime: string): Date => {
  const year = dateTime.slice(0, 4);
  const month = dateTime.slice(4, 6);
  const day = dateTime.slice(6, 8);
  const hour = dateTime.slice(9, 11);
  const minute = dateTime.slice(11, 13);

  return new Date(`${year}-${month}-${day}T${hour}:${minute}:00`);
};

export const addHours = (date: Date, hours: number): Date => {
  return new Date(date.getTime() + hours * 60 * 60 * 1000);
};

const generateRandomId = () => {
  return Math.floor(Math.random() * 1000000);
};

export const getTZOffset = (tzname: string): number => {
  const match = tzname.match(/GMT([+-]\d+)/);
  return match ? parseInt(match[1], 10) : 0;
};

export const getICalData = (event_id: number, title: string, icalData: string): IEventTime[] => {
  const events = [];
  const eventRegex = /BEGIN:VEVENT[\s\S]*?END:VEVENT/g;

  let match;
  let index = 0;
  while ((match = eventRegex.exec(icalData)) !== null) {
    const eventData = match[0];

    const start_time_str = parseNextWord(eventData, 'DTSTART:')!;
    const end_time_str = parseNextWord(eventData, 'DTEND:')!;
    const rrule = parseNextWord(eventData, 'RRULE:');
    const tzname = parseNextWord(eventData, 'TZNAME:') || 'GMT+3';
    const id = generateRandomId();
    const work_id = parseNextWord(eventData, 'WORKID:') || 'weekday';

    let start_time = formatDateTime(start_time_str);
    let end_time = formatDateTime(end_time_str);

    const tzOffset = getTZOffset(tzname);

    start_time = addHours(start_time, tzOffset);
    end_time = addHours(end_time, tzOffset);

    const formattedStartTime = `${start_time.getFullYear()}-${String(start_time.getMonth() + 1).padStart(2, '0')}-${String(start_time.getDate()).padStart(2, '0')} ${String(start_time.getHours()).padStart(2, '0')}:${String(start_time.getMinutes()).padStart(2, '0')}`;
    const formattedEndTime = `${end_time.getFullYear()}-${String(end_time.getMonth() + 1).padStart(2, '0')}-${String(end_time.getDate()).padStart(2, '0')} ${String(end_time.getHours()).padStart(2, '0')}:${String(end_time.getMinutes()).padStart(2, '0')}`;

    events.push({
      event_id: event_id,
      title: title,
      index: index,
      id: id,
      start: formattedStartTime,
      end: formattedEndTime,
      rrule: rrule,
      calendarId: work_id
    });

    index++;
  }

  return events;
};
