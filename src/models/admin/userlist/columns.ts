import { h } from 'vue';
// @ts-ignore
import { ColumnDef } from '@tanstack/vue-table';
import { IUser } from '@/api/userslist.api';

export const columns: ColumnDef<IUser>[] = [
  {
    accessorKey: 'last_name',
    header: () => h('div', { class: 'text-right' }, 'Фамилия'),
    cell: ({ row }: any) =>
      h('div', { class: 'text-right' }, row.getValue('last_name')),
  },
  {
    accessorKey: 'first_name',
    header: () => h('div', { class: 'text-right' }, 'Имя'),
    cell: ({ row }: any) =>
      h('div', { class: 'text-right' }, row.getValue('first_name')),
  },
  {
    accessorKey: 'middle_name',
    header: () => h('div', { class: 'text-right' }, 'Отчество'),
    cell: ({ row }: any) =>
      h('div', { class: 'text-right' }, row.getValue('middle_name')),
  },
  {
    accessorKey: 'place_name',
    header: () => h('div', { class: 'text-right' }, 'Место'),
    cell: ({ row }: any) =>
      h('div', { class: 'text-right' }, row.getValue('place_name')),
  },
  {
    accessorKey: 'email',
    header: () => h('div', { class: 'text-right' }, 'Электронная почта'),
    cell: ({ row }: any) =>
      h('div', { class: 'text-right' }, row.getValue('email')),
  },
  {
    accessorKey: 'phone_number',
    header: () => h('div', { class: 'text-right' }, 'Номер телефона'),
    cell: () =>
      h('div', { class: 'text-right' }, '88005553535'),
  },
];
