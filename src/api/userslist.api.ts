import axios from 'axios';
import { apiUrl } from '@/router/router';

export interface IUser {
  id: number;
  last_name: string;
  first_name: string;
  middle_name: string;
  place: number;
  place_name: string;
  phone_number: string;
  email: string;
}

export const getUsers = async (): Promise<IUser[]> => {
  try {
    const response = await axios.get<IUser[]>(`${apiUrl}/api/v1/users/`);
    return response.data;
  } catch (error) {
    console.error('Error fetching users:', error);
    throw new Error('Failed to fetch users');
  }
};
