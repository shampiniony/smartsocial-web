import axios from 'axios';
import { auth } from '@/store/auth.store';
import { apiUrl } from '@/router/router';

interface LoginResponse {
  access: string;
}

export const login = async (email: string, password: string): Promise<LoginResponse> => {
  try {
    const response = await axios.post<LoginResponse>(`${apiUrl}/api/v1/users/token/`, {
      email,
      password,
    });

    const { access } = response.data;
    auth.authenticated = true;
    auth.accessToken = access;
    axios.defaults.headers.common['Authorization'] = `Bearer ${auth.accessToken}`;

    localStorage.setItem('auth', JSON.stringify(auth));

    return response.data;
  } catch (error) {
    console.error('Error during login:', error);
    throw new Error('Invalid username or password');
  }
};

export const logout = () => {
  auth.authenticated = false;
  auth.accessToken = null;
  delete axios.defaults.headers.common['Authorization'];
  localStorage.removeItem('auth');
};
