import { reactive, watch } from 'vue';

interface AuthStore {
  authenticated: boolean;
  accessToken: string | null;
}

const loadAuthFromLocalStorage = (): AuthStore => {
  const savedAuth = localStorage.getItem('auth');
  if (savedAuth) {
    try {
      return JSON.parse(savedAuth);
    } catch (e) {
      console.error('Failed to parse auth from localStorage:', e);
    }
  }
  return {
    authenticated: false,
    accessToken: null,
  };
};

export const auth = reactive<AuthStore>(loadAuthFromLocalStorage());

watch(
  () => auth,
  (newAuth) => {
    localStorage.setItem('auth', JSON.stringify(newAuth));
  },
  { deep: true }
);

export const isAuthenticated = () => auth.authenticated;
