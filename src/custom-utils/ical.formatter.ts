export const formatDateTimeForICal = (dateTime: string) => {
  const date = new Date(dateTime);

  return toTimeForICal(date);
};

export const toTimeForICal = (date: Date) => {
  const year = date.getFullYear().toString().padStart(4, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  const seconds = date.getSeconds().toString().padStart(2, '0');

  return `${year}${month}${day}T${hours}${minutes}${seconds}`; 
}

export interface ICalData {
  title: string;
  start_time: string;
  end_time: string;
}

export const generateICal = (iCalData: ICalData): string => {
  const start_time = formatDateTimeForICal(iCalData.start_time);
  const end_time = formatDateTimeForICal(iCalData.end_time);
  const summary = iCalData.title

  return `BEGIN:VCALENDAR
  CALSCALE:GREGORIAN
  PRODID:-//Apple Inc.//macOS 14.5//EN
  VERSION:2.0
  X-APPLE-CALENDAR-COLOR:#CC73E1
  X-WR-CALNAME:Test
  BEGIN:VTIMEZONE
  TZID:Europe/Moscow
  BEGIN:STANDARD
  TZNAME:GMT+3
  TZOFFSETFROM:+023017
  TZOFFSETTO:+023017
  END:STANDARD
  END:VTIMEZONE
  BEGIN:VEVENT
  CREATED:20240615T061134Z
  DTEND:${end_time}
  DTSTAMP:20240615T061215Z
  DTSTART:${start_time}
  LAST-MODIFIED:20240615T061148Z
  RRULE:FREQ=DAILY;COUNT=1
  SEQUENCE:1
  SUMMARY:${summary}
  TRANSP:OPAQUE
  UID:7F367739-2CD1-490F-84A4-E8B8C35756B5
  X-APPLE-CREATOR-IDENTITY:com.apple.calendar
  X-APPLE-CREATOR-TEAM-IDENTITY:0000000000
  END:VEVENT
  END:VCALENDAR
`
}